/*
 * **************************************************************************
 * Copyright © 2013-2015 Jay Kamat
 *
 * Authors: Jay Kamat <onionchesse-jplanet@yahoo.com>
 *
 * This file is part of JayLayer.
 *
 * JayLayer is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JayLayer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with JayLayer.  If not, see <http://www.gnu.org/licenses/>.
 ****************************************************************************/

package io.github.jgkamat.JayLayer;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

/**
 * A class to hold sound effects
 */
class SoundEffectHolder {
	ArrayList<Song> backing;

	public SoundEffectHolder() {
		backing = new ArrayList<Song>();
	}

	public SoundEffectHolder(Collection<Song> toAdd) {
		this();
		this.addAll(toAdd);
	}

	public SoundEffectHolder(Song[] toAdd) {
		this();
		this.addAll(toAdd);
	}

	public void add(Song toAdd) {
		backing.add(toAdd);
	}

	public void addAll(Song[] toAdd) {
		Collections.addAll(backing, toAdd);
	}

	public void addAll(Collection<Song> toAdd) {
		backing.addAll(toAdd);
	}

	public Song get(int index) {
		return backing.get(index);
	}

	public Song get(String path) {
		for (Song in: backing) {
			if (in.getPath().equals(path)) {
				return in;
			}
		}
		return null;
	}

	public Song get(Song toGet) {
		int toSearch = backing.indexOf(toGet);
		if (toSearch < 0) {
			return null;
		}
		return backing.get(toSearch);
	}

	public int size() {
		return backing.size();
	}
}

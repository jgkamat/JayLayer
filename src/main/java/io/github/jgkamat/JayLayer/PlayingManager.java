/*
 * **************************************************************************
 * Copyright © 2013-2015 Jay Kamat
 *
 * Authors: Jay Kamat <onionchesse-jplanet@yahoo.com>
 *
 * This file is part of JayLayer.
 *
 * JayLayer is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JayLayer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with JayLayer.  If not, see <http://www.gnu.org/licenses/>.
 ****************************************************************************/

package io.github.jgkamat.JayLayer;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

/**
 * Manages currently playing songs
 */
class PlayingManager {
	private Map<UUID, MP3Player> playing;
	private JayLayer top;

	public PlayingManager(JayLayer top) {
		playing = new HashMap<UUID, MP3Player>();
		this.top = top;
	}

	public UUID add(Song song, float volume) {
		MP3Player player = new MP3Player(song, this);
		player.updateVolume(volume);
		playing.put(player.getUUID(), player);
		this.play(player.getUUID());

		return player.getUUID();
	}

	public void notifyDone(UUID id) {
		top.notifyDone(playing.get(id).getSong());
		playing.remove(id);
	}

	public void play(UUID id) {
		playing.get(id).play();
	}

	public void kill(UUID toKill) {
		if (toKill == null) {
			throw new IllegalArgumentException("UUID was null!");
		}
		// This method will call notifyDone, which will remove it from the map
		playing.get(toKill).kill();
	}

	/**
	 * Stops the song without telling anyone about it being stopped
	 * @param toKill
	 */
	public void silentKill(UUID toKill) {
		if (toKill == null) {
			throw new IllegalArgumentException("UUID was null!");
		}
		// This method will call notifyDone, which will remove it from the map
		if (playing.get(toKill) != null) {
			playing.get(toKill).silentKill();
			playing.remove(toKill);
		}
	}

	public Song getSong(UUID id) {
		return  playing.get(id).getSong();
	}

	public void setAllVolumes(float toSet) {
		for(MP3Player loopy: playing.values()) {
			loopy.updateVolume(toSet);
		}
	}

	public void setVolume(UUID id, float toSet) {
		playing.get(id).updateVolume(toSet);
	}
}

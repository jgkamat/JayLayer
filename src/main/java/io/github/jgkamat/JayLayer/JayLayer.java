/*
 * **************************************************************************
 * Copyright © 2013-2015 Jay Kamat
 *
 * Authors: Jay Kamat <github@jgkamat.33mail.com>
 *
 * This file is part of JayLayer.
 *
 * JayLayer is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JayLayer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with JayLayer.  If not, see <http://www.gnu.org/licenses/>.
 ****************************************************************************/

package io.github.jgkamat.JayLayer;

import java.util.*;

/**
 * JSoundPlayer
 *
 * JSoundPlayer is a library based off the JLayer library, to enable Java beginners to use it.
 *
 * This library is only tested with standard mp3 files!
 *
 * This player is based off of playlists and sound effects.
 * Playlists are designed for background music, which constantly plays, while sound effects
 * are one-time-only sounds, that can be called when needed.
 *
 * @author Jay Kamat
 * @version 4.1
 * Manages the playback for MP3 files
 *
 * Released under the Lesser GNU Lesser General Public License V3.0
 *
 * This program uses an unmodified version of the JLayer library (1.0.1), it is just bundled inside for convenience, if you obtain it from the github.
 * @see <a href="http://www.javazoom.net/javalayer/javalayer.html">JLayer Library</a>
 *
 *
 * Copyright Jay Kamat, 2015
 */
public class JayLayer {

    private ArrayList<Playlist> backgroundSounds;
	SoundEffectHolder soundEffects;
	private String playlistPrefix;
	private String effectPrefix;
	private PlayingManager manager;
	private float masterVolume;
	private float effectVolume;

	/**
	 * This constructor assumes all sound files are in the src directory
	 */
	public JayLayer() {
		this.playlistPrefix = "";
		this.effectPrefix = "";
		this.backgroundSounds = new ArrayList<Playlist>();
		this.soundEffects = new SoundEffectHolder();
		this.manager = new PlayingManager(this);
		this.masterVolume = 1f;
		this.effectVolume = 1f;
	}

	/**
	 * This constructor allows for prefixing song names. For example, placing /audio/ here
	 * will assume the files for that type are placed in src/audio.
	 * @param playlistPrefix The prefix for playist files.
	 * @param effectPrefix The prefix for sound-effects.
	 */
	public JayLayer(String playlistPrefix, String effectPrefix) {
		this();
		this.playlistPrefix = playlistPrefix;
		this.effectPrefix = effectPrefix;
	}

	/**
	 * Creates a playlist that can play background sounds.
	 * @param autoShuffle Whether the playlist should be played in order, or shuffled.
	 * @return The integer index of this playlist. This will be used when starting this playlist.
	 */
	public int createPlaylist(boolean autoShuffle) {
		backgroundSounds.add(new Playlist(autoShuffle, manager));
		return backgroundSounds.size() - 1;
	}

	/**
	 * Adds a song name to an existing playlist
	 * @param playlistNumber The playlist index to add to.
	 * @param songName The song's filename which will be prefixed with what was set in the constructor.
	 * @return The numerical index of this song, within the playlist
	 */
	public int addToPlaylist(int playlistNumber, String songName) {
		backgroundSounds.get(playlistNumber).add(new Song(playlistPrefix + "" + songName, playlistNumber));
		return backgroundSounds.get(playlistNumber).size() - 1;
	}

	/**
	 * Adds a sound effect to the sound effect list
	 * @param songName The filename for the sound effect, which will be prefixed with what was set
	 * in the constructor.
	 * @return The integer value of this sound effect
	 */
	public int addSoundEffect(String songName) {
		soundEffects.add(new Song(effectPrefix + "" + songName, -1));
		return soundEffects.size() - 1;
	}

	/**
	 * Plays a sound effect.
	 * @param index The index of the sound effect to play
	 * @return An identifier to which song instance is playing. This can be used later to stop/pause an instance.
	 */
	public UUID playSoundEffect(int index) {
		return manager.add(soundEffects.get(index), this.getMaster() * this.getEffectVolume());
	}

	/**
	 * Destroys a playing sound effect
	 * @param toKill The UUID of the sound effect to kill.
	 */
	public void killSoundEffect(UUID toKill) {
		manager.kill(toKill);
	}

	/**
	 * Starts playing a playlist.
	 * @param toStart The integer value of the playlist to play.
	 * @return An identifier to the first song that is playing in the playlist. This identifer will change
	 * for the next song being played.
	 */
	public UUID startPlaylist(int toStart) {
		return backgroundSounds.get(toStart).playNext(getMaster());
	}

	/**
	 * Starts playing a new playlist. Will stop any other playing playlists.
	 * If this playlist is already started, all other playlist's playback will be stopped, and this playlist will not be affected
	 * @param toStart The integer value of the playlist to play.
	 * @return An identifier to the song that is playing in the playlist. This identifer will change
	 * for the next song being played.
	 */
	public UUID changePlaylist(int toStart) {
		for (Playlist loop : backgroundSounds) {
			if (loop != backgroundSounds.get(toStart))
				loop.stop();
		}
		// Start if not already started
		if (backgroundSounds.get(toStart).getPlaying() == null) {
			return backgroundSounds.get(toStart).playNext(getMaster());
		}
		return backgroundSounds.get(toStart).getPlaying();
	}

	public void stopAllPlaylists() {
		for (Playlist loop : backgroundSounds) {
			loop.stop();
		}
	}

	/**
	 * Stops playing this playlist. The playback cannot be resumed from this point, it can only be restarted from the beginning of the song.
	 * @param toStop The integer value of the playlist to stop.
	 * @return An identifier of the song object that has been stopped.
	 */
	public UUID stopPlaylist(int toStop) {
		return backgroundSounds.get(toStop).stop();
	}

	/**
	 * Gets the filename of the song that is currently being played.
	 * @param playlist The integer valu eof the playlist to query
	 * @return The filename requested
	 */
	public String getFilenamePlaying(int playlist) {
		return backgroundSounds.get(playlist).getFilenamePlaying();
	}

	/**
	 * Gets the filename that matches an identifier.
	 * @param id The ID to search for
	 * @return The filename that was found.
	 */
	public String getFilename(UUID id) {
		return manager.getSong(id).getPath();
	}

	UUID notifyDone(Song name) {
		if (name.isInPlaylist()) {
			return backgroundSounds.get(name.getPlaylist()).playNext(getMaster());
		}
		return null;
	}

	/*
	 Volume adjustments. Currently could be unstable.
	 */

	/**
	 * Sets the master volume for all new songs.
	 * @param volume A float with values from 0 to 1.
	 */
	public void setMasterVolume(float volume) {
		masterVolume = volume;
	}

	/**
	 * Sets the master volume for all songs playing.
	 * @param volume A float with values from 0 to 1.
	 */
	public void setPlayingVolume(float volume) {
		manager.setAllVolumes(volume);
	}

	/**
	 * Sets the volume for a single instance of a song playing, by identifier.
	 * @param volume A float with values from 0 to 1.
	 * @param id The id of the song to change the volume of.
	 */
	public void setVolume(UUID id, float volume) {
		manager.setVolume(id, volume);
	}

	/**
	 * Sets the volume for all future songs played in a playlist.
	 * @param playlist The playlist to modify.
	 * @param volume A float with values from 0 to 1
	 */
	public void setPlaylistVolume(int playlist, float volume) {
		backgroundSounds.get(playlist).setVolume(volume);
	}

	/**
	 * Returns the playlist's set volume.
	 * @param playlist The playlist to query.
	 * @return A float from 0 to 1 determining the volume.
	 */
	public float getPlaylistVolume(int playlist) {
		return backgroundSounds.get(playlist).getVolume();
	}

	/**
	 * Sets the volume set for all future sound effects
	 * @param volume A float from 0 to 1 determining the volume.
	 */
	public void setSoundEffectVolume(float volume) {
		effectVolume = volume;
	}

	/**
	 * Returns the volume for all future sound effects.
	 * @return A float from 0 to 1 representing volume for all sound effects.
	 */
	public float getEffectVolume() {
		return effectVolume;
	}

	/**
	 * Returns the master volume.
	 * @return A float from 0 to 1 representing the master volume.
	 */
	public float getMaster() {
		return masterVolume;
	}

}

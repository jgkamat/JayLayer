/*
 * **************************************************************************
 * Copyright © 2013-2015 Jay Kamat
 *
 * Authors: Jay Kamat <onionchesse-jplanet@yahoo.com>
 *
 * This file is part of JayLayer.
 *
 * JayLayer is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JayLayer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with JayLayer.  If not, see <http://www.gnu.org/licenses/>.
 ****************************************************************************/

package io.github.jgkamat.JayLayer.test;

/*

This class is a sample class to show an example of how to use the library

There are both advanced and simple features displayed in this file!

*/

import io.github.jgkamat.JayLayer.JayLayer;

import java.util.UUID;

public class JayLayerTest {

	//TODO reimplement io.github.jgkamat.JayLayer.test file

	public static void main(String[]args){
		UUID uuid;
		JayLayer sound=new JayLayer("/audio/", "/audio/");

		// A new playlist is added, and songs are added to the playlist. This playlist will cycle through the songs randomly
		int playlistNum = sound.createPlaylist(true);
		sound.addToPlaylist(playlistNum, "file1.mp3");
		sound.addToPlaylist(playlistNum, "file2.mp3");
		sound.addToPlaylist(playlistNum, "file3.mp3");
		sound.addToPlaylist(playlistNum, "file4.mp3");
		//sound.addSong(0, "file5.mp3");

		// This is the time in between each song playing. Set to 0 for near-seamless music
		// TODO reimplement setWait
		// sound.setWait(1000);

		//the playlist will now start playing sound!
		sound.startPlaylist(0);

		// TODO reimplment get song name
		// System.out.println(sound.getCurrentSongName());

        try {
            Thread.sleep(4000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }


		System.out.println("Changing volume on the fly");
		sound.setMasterVolume(0.5f);
		// This will forcibly, but temporarily change all playing sound volumes
		sound.setPlayingVolume(0.5f);

		//A wait of 4 seconds so you can distinguish different parts of this test scenario
		try {
			Thread.sleep(4000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		// The song is advanced
		// TODO implement nextSong
		// sound.changeSound();
		// System.out.println(sound.getCurrentPath());


		try {
			Thread.sleep(1000);
		} catch (InterruptedException e1) {
			e1.printStackTrace();
		}

		// Sound effects allow for you to play 1 time sound bursts, on top of the background music
		int soundEffect = sound.addSoundEffect("file5.mp3");
		int soundEffect2 = sound.addSoundEffect("file4.mp3");

		uuid = sound.playSoundEffect(soundEffect);
		System.out.println(sound.getFilename(uuid));

		try {
			Thread.sleep(250);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		uuid = sound.playSoundEffect(soundEffect2);
		System.out.println(sound.getFilename(uuid));

		UUID tokill = uuid;

		//A wait of 3 seconds so you can see how we can play the sound effect again
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		//Playing the same sound again
		uuid = sound.playSoundEffect(soundEffect);
		System.out.println(sound.getFilename(uuid));
		// temp volume change
		sound.setVolume(uuid, 0.5f);

		System.out.println(sound.getFilename(tokill) + " being killed!");
		sound.killSoundEffect(tokill);

		// The background sounds will now cycle, as one ends, the next one will begin!
		//System.exit(0) will stop both the program and the sound!
	}
}

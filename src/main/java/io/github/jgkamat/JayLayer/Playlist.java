/*
 * **************************************************************************
 * Copyright © 2013-2015 Jay Kamat
 *
 * Authors: Jay Kamat <onionchesse-jplanet@yahoo.com>
 *
 * This file is part of JayLayer.
 *
 * JayLayer is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JayLayer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with JayLayer.  If not, see <http://www.gnu.org/licenses/>.
 ****************************************************************************/

package io.github.jgkamat.JayLayer;

import java.util.*;

/**
 * A class to manage a list of songs
 */
class Playlist {

    private Deque<Song> backing;
    private List<Song> completed;
    private boolean shuffle;
	private UUID playing;
	private PlayingManager manager;
	private float playlistVolume;

    public Playlist(boolean shuffle, PlayingManager manager) {
        backing = new LinkedList<Song>();
        completed = new LinkedList<Song>();
        this.shuffle = shuffle;
		this.manager = manager;
		this.playlistVolume = 1f;
    }

    public Playlist(Collection<Song> toAdd, boolean shuffle, PlayingManager manager) {
        this(shuffle, manager);
        this.addAll(toAdd);
    }

    public Playlist(Song[] toAdd, boolean shuffle, PlayingManager manager) {
        this(shuffle, manager);
        this.addAll(toAdd);
    }

    public void add(Song toAdd) {
        backing.add(toAdd);
    }

    public void addAll(Song[] toAdd) {
		Collections.addAll(backing, toAdd);
    }

    public void addAll(Collection<Song> toAdd) {
        backing.addAll(toAdd);
    }

	public int size() {
		return backing.size() + completed.size();
	}

    public UUID playNext(float masterVolume) {
		checkEmpty();
        Song toReturn = backing.remove();
        completed.add(toReturn);
		playing = manager.add(toReturn, playlistVolume * masterVolume);
        return playing;
    }

	public UUID stop() {
		UUID oldPlaying = playing;
		if (playing != null)
			manager.silentKill(playing);
		playing = null;
		return oldPlaying;
	}

	public UUID getPlaying() {
		return playing;
	}

	public String getFilenamePlaying() {
		return manager.getSong(playing).getPath();
	}

    private void checkEmpty() {
		if (backing.size() == 0 && completed.size() == 0) {
			throw new NoSuchElementException("No elements were in this playList!");
		}

        if (backing.size() == 0) {
			if (shuffle) {
				Collections.shuffle(completed);
			}

			while (completed.size() > 0) {
				backing.add(completed.remove(0));
			}
		}
    }

	public void setVolume(float volume) {
		this.playlistVolume = volume;
	}

	public float getVolume() {
		return playlistVolume;
	}
}

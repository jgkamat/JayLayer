package io.github.jgkamat.JayLayer;

/**
 * A class to represent a song
 */
class Song {
    private final String filePath;
	private int playist;

    public Song(String path, int playist) {
		this.playist = playist;
        this.filePath = path;
    }

    public String getPath() {
        return filePath;
    }

	@Override
	public boolean equals(Object o) {
		if (o == this) {
			return true;
		} else if (o instanceof Song) {
			Song other = (Song) o;
			if (other.filePath.equals(this.filePath)) {
				return true;
			}
		}
		return false;
	}

	public boolean isInPlaylist() {
		return playist > -1;
	}

	public int getPlaylist() {
		return playist;
	}
}

/*
 * **************************************************************************
 * Copyright © 2013-2015 Jay Kamat
 *
 * Authors: Jay Kamat <onionchesse-jplanet@yahoo.com>
 *
 * This file is part of JayLayer.
 *
 * JayLayer is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JayLayer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with JayLayer.  If not, see <http://www.gnu.org/licenses/>.
 ****************************************************************************/

package io.github.jgkamat.JayLayer;

import java.io.FileInputStream;
import java.io.InputStream;
import java.lang.reflect.Field;
import java.util.UUID;

import javax.sound.sampled.Control;
import javax.sound.sampled.FloatControl;
import javax.sound.sampled.Control.Type;
import javax.sound.sampled.SourceDataLine;

import javazoom.jl.player.Player;

/**
 *
 * @author Jay Kamat
 * @version 4.0
 *
 */
class MP3Player {

    private Player player;
    private InputStream is;
    private PlayingManager top;
    private PlayerThread pt;
    private SourceDataLine source;
	private UUID uuid;
	private Song song;


    public MP3Player(Song filename, PlayingManager playlistHandler) {
		this.song = filename;
        top = playlistHandler;
        is = getClass().getResourceAsStream(this.song.getPath());

        if(is==null){
            throw new IllegalArgumentException("\n\nThe Filename: "+filename+" didn't quite check out!\n"
                    + "Try seeing if it still exists, or if something else is wrong with the file or path.\n");
        }

		uuid = UUID.randomUUID();
        // for a more "outer" root directory use this
        // is = new FileInputStream( filename );
    }

	public Song getSong() {
		return song;
	}

	public UUID getUUID() {
		return uuid;
	}

    public UUID play() {
        try {
            player = new Player(is);
            pt = new PlayerThread();
            pt.start();
        } catch (Exception e) {
            e.printStackTrace();
        }
		return uuid;
    }

	/**
	 * A kill method that does not tell anyone about its death
	 */
	public void silentKill() {
		player.close();
	}

	public void kill() {
		silentKill();
		if (top != null)
			top.notifyDone(uuid);
	}

	public boolean done() {
		return player.isComplete();
	}

    public FloatControl getGainControl(){
        return (FloatControl) getControl(FloatControl.Type.MASTER_GAIN);
    }

    public boolean isControlSupported(Type type){
        initializeSource();
        return source.isControlSupported(type);
    }

    public Control getControl(Type type){
        initializeSource();
        if (source == null) // Not yet initialized
            return null;

        return source.getControl(type);
    }

    public Control[] getControls(){
        return source.getControls();
    }

    private void initializeSource() {
        if (source == null)
            try {
                // Get 'audio', a javazoom.jl.player.AudioDevice object
                Field f = player.getClass().getDeclaredField("audio");
                f.setAccessible(true);
                Object o = f.get(player);
                // It should be an implementation of JavaSoundAudioDevice
                // get 'source'. This is a javax.sound.sampled.SourceDataLine
                // It will allow us to adjust the volume.
                f = o.getClass().getDeclaredField("source");
                f.setAccessible(true);
                source = (SourceDataLine) f.get(o);

            } catch (Exception e) {
                if(e instanceof RuntimeException){
                    throw (RuntimeException)  e;
                } else {
                    System.err.println(
                            "!!! There was a critical error in the sound processing. !!!\n"
                            + "    -Please file a bug report including the following\n"
                            + "     StackTrace, and if possible the steps to repeat this.\n"
                            + "*This error message should never be seen*");
                    e.printStackTrace();
                    throw new RuntimeException(e);
                }
            }
    }

    class PlayerThread extends Thread {
        public void run() {
            try {
				player.play();
				if (player.isComplete()) {
					if (top != null) {
						top.notifyDone(uuid);
					}
				}
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void updateVolume(float rawVolume) {
        if(rawVolume < 0 || rawVolume > 1)
            throw new IllegalArgumentException("volume must be between 0.0f & 1.0f, and was '" + rawVolume + "'");
		// Things seem to quiet. Maybe theres some sound physics we're forgetting.
		final float volume = (float) Math.pow(rawVolume, 0.25);
		// System.out.println(volume);
        try {
            protectedVolumeUpdate(volume);
        } catch(Exception e) {
            // If there was a problem, there's a good chance that it just hasn't
            // had the chance to warm up yet; This will give it that chance.

            new Thread(){
                @Override
                public void run(){
                    Exception e = null;
                    //KEEP TRYING!
                    for(int i = 0; i < 100; i++){
                        boolean noProblem = true;
                        try {
                            Thread.sleep(10);
                            protectedVolumeUpdate(volume);
                        } catch (Exception e2) {noProblem = false; e = e2;}
                        if(noProblem)
                            return;
                    }
                    //It's tried to warm up for a full second, but no cigar.
                    //We're calling it quits.
                    throw new RuntimeException(
                            "Unable to set the volume. This shouldn't happen. Please file a bug report:",
                            e);
                }
            }.start();
        }
    }

    private void protectedVolumeUpdate(float volume){
        FloatControl fc = getGainControl();
        float range = fc.getMaximum() - fc.getMinimum();
        fc.setValue(fc.getMinimum() + volume * range);
    }
}
